package ltd;

class Customer {
	private int id, cardNomber;
	private String name,surName;

	public Customer(){

	}

	public Customer(String name, String surName, int id, int cardNomber){

		setName(name);
		setLastName(surName);
		setCardNomber(cardNomber);
		setId(id);
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCardNomber(int cardNomber) {
		this.cardNomber = cardNomber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String surName) {
		this.surName = surName;
	}

	public int getId() {
		return id;
	}

	public int getCardNomber() {
		return cardNomber;
	}

	public String getName() {
		return name;
	}

	public String getLastName(String lastName) {
		return lastName;
	}

	private Customer createCustomer(){
		return new Customer(randomChar(10), randomChar(8), randomNum(10), randomNum(16));
					//		Customer (String name, String surName, int id, int cardNomber)	 
	}
	
	
	public Customer[] createArrayCust(int qty){
		Customer[] customers = new Customer[qty];
				for (int i=0; i<qty; i++) {
					customers[i] = createCustomer();
					//		Customer (String name, String surName, int id, int cardNomber)
				}
		return customers;
	}

	String randomChar(int qty){
		StringBuilder name = new StringBuilder (qty);
		for (int i=0; i<qty ;i++) {
			name.append(randomNum(24)+65);
		}

		 return name.toString();		
	}

	static int randomNum(int qty){
		return (int) (Math.random()*qty);	
	}
	

}
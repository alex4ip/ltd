package SemaphorDemo;

import java.util.concurrent.*;

public class Sort {
	
	public static final int ITEMS_COUNT = 15;
	public static int items[];
	
	public static Semaphore sortSemaphore = new Semaphore(0, true);

	public static void main(String[] args) {
		items = new int[ ITEMS_COUNT];
		for(int i = 0 ; i < items. length ; ++i)
			items[i] = (int)(Math.random()*10);
		new Thread(new ArraySort(items)).start();
		for(int i = 0 ; i < items.length ; ++i) {
			sortSemaphore.acquireUninterruptibly();
			System. out.println(items[i]);
		}
	}
}

class ArraySort implements Runnable {

	private int items[];

	public ArraySort(int items[]) {
		this.items = items;
	}

	public void run(){
		for(int i = 0 ; i < items.length - 1 ; ++i) {
			for(int j = i + 1 ; j < items.length ; ++j) {
				if( items[i] < items[j] ) {
					int tmp = items[i];
					items[i] = items[j];
					items[j] = tmp;
				}
			}
			Sort.sortSemaphore.release();

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System. err.print(e);
			}
		}
		Sort.sortSemaphore.release();
	}
}
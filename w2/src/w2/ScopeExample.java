package w2;

public class ScopeExample {

	private int i = 1;
	
	public void pp() {
		System.out.println(i);
	}

	public void firstMethod() {

		int i = 4, j = 5;
		this.i = i + j;
		pp();
		secondMethod(7);
		pp();
	}
	
	public void secondMethod(int i) {
		int j = 8;
		this.i = i + j;
		pp();
		}

	public static void main(String[] args) {
		ScopeExample scope = new ScopeExample();
		scope.firstMethod();
	}

}
